import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <a
          className="App-link"
          href="https://developer.mozilla.org/en-US/"
          target="_blank"
          rel="noopener noreferrer"
        >
          In Case
        </a>
      </header>
    </div>
  );
}

export default App;
